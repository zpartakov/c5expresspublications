# c5expressPublications

[![c5expressPublications](//radeff.red/pics/c5/c5expressPublications.png)](c5expressPublications)

Scientific bibliography for concrete5's website, using Express module, displaying links to inlines PDF and web url's

## Required
- concrete5, version >= 8.5.0

## Install
put the content of this repo under application/blocks/express_entry_list/templates/publications/view.php

## Usage

Create concrete5's express object with  following attributes handles:

 - auteurs (text)
 - titre (text)
 - annee (text)
 - pdf (Image/File)
 - url (text)

Populate the express object with your bibiography

Insert an express list on the page you need

Enjoy!
